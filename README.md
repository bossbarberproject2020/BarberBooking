# Barber
Deploy to: barber-shop.meteor.com
## Directory Structure
The application will have the following directory structure:

```sh
barber/
  .meteor/
  client/
    lib/
    styles/
    [object]/
      controllers/
      filters/
      styles/
      views/
    index.html
    routes.ng.js
  model
  packages/
  public/
  resources/
    icons/
  server/
    startup
```
