angular.module("barber-booking").controller("BookingCtrl", ['$scope', '$meteor', '$modal', '$filter', '$state', 'confirmService', 'Flash',
  function ($scope, $meteor, $modal, $filter, $state, confirmService, Flash) {

    var self = this;

  /* Initialize date */
    $scope.today = moment().toDate();
    $scope.currentDate = moment().format("YYYY-MM-DD");
    $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
    $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
  /******************/

  /* Initialize header date picker*/
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 0
    };

    $scope.barberScope = function(){
      $scope.currentDate = moment($scope.today).format("YYYY-MM-DD");
      $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
      $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
    }
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0) );
    };

    $scope.getMainImage = function(images) {
      if (images && images.length && images[0] && images[0].id) {
        img = $filter('filter')($scope.images, {_id: images[0].id})[0];
        return (img ? img.url() : 'no-photo.png');
      } else {
        if(images == 'anyone'){
          return 'anyone-avatar.png'
        }else{
          return 'no-photo.png';
        }
      }
    };

    $scope.goAdmin = function() {
      $state.go('login');
    }

    $scope.goSchedule = function() {
      $state.go('schedule');
    }
  /******************************/

  $scope.work_hours = WORK_HOURS;

  /***** Subscribe Functions *****/
  $scope.images = $meteor.collectionFS(Images, false, Images).subscribe('images');

  $scope.$meteorAutorun(function() {

    /***** Subscribe appointments *****/
    $scope.$meteorSubscribe('appointments', $scope.getReactively('currentStartDate'), $scope.getReactively('currentEndDate')).then(function() {
      $scope.appointments = $scope.$meteorCollection(function() {
        return Appointments.find({
          start_time: {$gte: $scope.getReactively('currentStartDate')},
          start_time: {$lt: $scope.getReactively('currentEndDate')}
        });
      });
    });

    /***** Subscribe barbers *****/
    $scope.$meteorSubscribe('barbers', $scope.getReactively('currentEndDate')).then(function() {
      $scope.barbers = $scope.$meteorCollection(function(){
        return Barbers.find({
          start_date: { $lte: $scope.getReactively('currentEndDate')}
        });
      });
    });

  });

  $meteor.autorun($scope, function() {
    if($scope.getCollectionReactively('barbers')) {
      $scope.has_anyone = $scope.barbers.length > 0;
      reLoadAppointment();
    }
  })

  $meteor.autorun($scope, function() {
    if($scope.getCollectionReactively('appointments')) {
      if($scope.getReactively("currentBarber")) {
        reLoadAppointment();
      }
    }
  })

  /*******************************/

  /***** Barber Functions *****/
   $scope.selectBarber = function(barber) {
    if(barber == 'anyone') {
      $scope.currentBarber = {
        _id: 'anyone',
        name: 'Anyone',
        images: 'anyone'
      }
    }else{
      $scope.currentBarber = barber;
    }
    reLoadAppointment();
  }

  $scope.bookAppointment = function(hour) {
    console.log(hour);

    if(!$scope.currentBarber) {
      return;
    }

    var modalConfirm = $modal.open({
      animation: true,
      windowClass: "new-customer-modal normal-modal",
      templateUrl: 'client/booking/views/confirm-booking.html',
      controller: 'ConfirmBookingCtrl'
    });

    modalConfirm.result.then(function(customer){
      console.log("Booked");
      if (_.isEmpty(customer)) {
        customer = {
          name: "",
          phone: ""
        }
      } else {
        status = "booked";
      }

      book_time = hour.split(" - ");
      start_time = book_time[0].split(":");
      end_time = book_time[1].split(":");

      new_appointment =  {
        barber_id: $scope.currentBarber._id,
        start_time: moment.utc($scope.currentStartDate).hour(start_time[0]).minute(start_time[1])._d,
        end_time: moment.utc($scope.currentStartDate).hour(end_time[0]).minute(end_time[1])._d,
        status: "booked",
        time: hour,
        cus_name: customer.name,
        cus_phone: customer.phone
      };

      $meteor.call('newBookingAppointment', new_appointment).then(function(){
        Flash.create('success', 'Booked!!!', '');
      }, function(error){
        Flash.create('danger', error.reason, '');
      })
    }, function(){
      console.log("Cancel");
    });
  }

  function reLoadAppointment() {
    if (!$scope.currentBarber) {
      return;
    }

    $scope.currentCalendars = [];
    var get_day = $scope.today.getDay(),
        hours_count =  $scope.work_hours.length;
    for (var i = 0; i < hours_count; i++) {
      if (check_off(get_day, i)) {
        appointment = "off";
      } else{
        appointment = hasAppointment($scope.work_hours[i]);
      }
      $scope.currentCalendars.push({
        time: $scope.work_hours[i],
        appointment: appointment
      });
    };
  }

  function check_off(numDay, i) {
    switch(numDay) {
      case 0:
        return true;
      case 1:
      case 2:
      case 3:
      case 4:
        if(i < 2) {
          return true;
        }
        return false;
      case 5:
        return false;
      case 6:
        if (i >= WORK_HOURS.length - 2) {
          return true;
        }
        return false;
      default:
        return false
    }
  }

  function hasAppointment(hour) {
    var book_time = hour.split(" - ");
    var start_time = book_time[0].split(":");
    var end_time = book_time[1].split(":");
    p_start_time = moment.utc($scope.currentStartDate).hour(start_time[0]).minute(start_time[1])._d;
    p_end_time = moment.utc($scope.currentStartDate).hour(end_time[0]).minute(end_time[1])._d;


    if($scope.currentBarber._id == 'anyone'){
      existed_appointment = Appointments.find({
        start_time: { $gte: p_start_time},
        start_time: { $lt: p_end_time},
        end_time: { $lte: p_end_time},
        end_time: { $gt: p_start_time}
      }).count() == $scope.barbers.length;
    }else{
      existed_appointment = Appointments.findOne({
        barber_id: $scope.currentBarber._id,
        start_time: { $gte: p_start_time},
        start_time: { $lt: p_end_time},
        end_time: { $lte: p_end_time},
        end_time: { $gt: p_start_time}
      })
    }

    return existed_appointment;
  }


}]);


