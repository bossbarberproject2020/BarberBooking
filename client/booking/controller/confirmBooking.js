angular.module('barber-booking').controller("ConfirmBookingCtrl", function($scope, $meteor, $rootScope, $state, $modalInstance){
  
  $scope.customer = {};

  $scope.saveCustomer = function(isValid){
    if(!isValid){
      $scope.submitted = true;
      return;
    }
    
    $modalInstance.close($scope.customer);  
  }

  $scope.cancel = function(){ 
    $modalInstance.dismiss('cancel');
  }
})
