angular.module("barber-booking").controller("ScheduleCtrl", ['$scope', '$meteor', '$modal', '$filter', '$state', 'confirmService', 'Flash',
  function ($scope, $meteor, $modal, $filter, $state, confirmService, Flash) {

    var self = this;

  /* Initialize date */
    $scope.today = moment().toDate();
    $scope.currentDate = moment($scope.today).format("YYYY-MM-DD");
    $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
    $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
  /******************/

  /* Initialize header date picker*/
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 0
    };

    $scope.barberScope = function(){
      $scope.currentDate = moment($scope.today).format("YYYY-MM-DD");
      $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
      $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
    }
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0) );
    };

    $scope.getMainImage = function(images) {
      if (images && images.length && images[0] && images[0].id) {
        img = $filter('filter')($scope.images, {_id: images[0].id})[0];
        return (img ? img.url() : 'no-photo.png');
      } else {
        if(images == 'anyone'){
          return 'anyone-avatar.png'
        }else{
          return 'no-photo.png';
        }
      }
    };

    $scope.goAdmin = function() {
      $state.go('login');
    }

    $scope.goBooking = function() {
      $state.go('walk');
    }
  /******************************/

  $scope.work_hours = WORK_HOURS;

  /***** Subscribe Functions *****/

  $scope.$meteorAutorun(function() {

    /***** Subscribe appointments *****/
    $scope.$meteorSubscribe('appointments', $scope.getReactively('currentStartDate'), $scope.getReactively('currentEndDate')).then(function() {
      $scope.appointments = $scope.$meteorCollection(function() {
        return Appointments.find({
          start_time: {$gte: $scope.getReactively('currentStartDate')},
          start_time: {$lt: $scope.getReactively('currentEndDate')}
        });
      });
    });

    /***** Subscribe barbers *****/
    $scope.$meteorSubscribe('barbers', $scope.getReactively('currentEndDate')).then(function() {
      $scope.barbers = $scope.$meteorCollection(function(){
        return Barbers.find({
          start_date: { $lte: $scope.getReactively('currentEndDate')}
        });
      });
    });

  });

  $meteor.autorun($scope, function() {
    if($scope.getCollectionReactively('barbers')) {
      $scope.has_anyone = $scope.barbers.length > 0;
      reLoadAppointment();
    }
  })

  $meteor.autorun($scope, function() {
    if($scope.getCollectionReactively('appointments')) {
      if($scope.getReactively("currentBarber")) {
        reLoadAppointment();
      }
    }
  })

  /*******************************/

  /***** Barber Functions *****/

  function reLoadAppointment() {
    if (!$scope.barbers) {
      return;
    }

    $scope.currentCalendars = [];
    var appointments = Appointments.find({
      $or: [
        {status: 'booked'},
        {status: 'assigned'}
      ]
    }, {sort: {start_time: 1}}).fetch();
    var appointments_count = appointments.length;
    for (var i = 0; i < appointments_count; i++) {
      var appointment = appointments[i];
      var barber = Barbers.findOne({_id: appointment.barber_id});
      var group_appointment = {
        barber_id: barber._id,
        barber_name: barber.name,
        appointment: appointment,
      }
      $scope.currentCalendars.push(group_appointment);
    }
  }


}]);


