angular.module('barber-booking').controller("VerificationCtrl", function($scope, $meteor, Flash, $modalInstance){

  $scope.customer = {};

  $scope.checkOff = function(code) {
    if (!code) {
      Flash.create('danger', "The code is not valid!", '');
      return;
    }
    var digest = Package.sha.SHA256(code);
    $meteor.call('verifyBarberCode', digest).then(function(res){
      if(res) {
        $modalInstance.close(true);
      }
      else {
        Flash.create('danger', "The code is not valid!", '');
      }
    }, function(error){
       Flash.create('danger', error.reason, '');
    });
  }

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  }
})