angular.module("barber-booking").controller("WalkCtrl", ['$scope', '$meteor', '$modal', '$filter', '$state', 'Flash',
  function ($scope, $meteor, $modal, $filter, $state, Flash) {

    var self = this;

  /* Initialize date */
    $scope.today = moment().toDate();
    $scope.currentDate = moment().format("YYYY-MM-DD");
    $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
    $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
    $scope.newBooking = {};
  /******************/

  /* Initialize header date picker*/
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 0
    };

    $scope.barberScope = function(){
      $scope.currentDate = moment($scope.today).format("YYYY-MM-DD");
      $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
      $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
    }
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0) );
    };

    $scope.goAdmin = function() {
      $state.go('login');
    }

    $scope.goSchedule = function() {
      $state.go('schedule');
    }
  /******************************/

  $scope.work_hours = WORK_HOURS;

  /***** Subscribe Functions *****/
  $scope.images = $meteor.collectionFS(Images, false, Images).subscribe('images');

  $scope.$meteorAutorun(function() {

    /***** Subscribe appointments *****/
    $scope.$meteorSubscribe('booking_appointments', $scope.getReactively('currentStartDate'), $scope.getReactively('currentEndDate')).then(function() {
      $scope.appointments = $scope.$meteorCollection(function() {
        return Appointments.find({
          start_time: {$gte: $scope.getReactively('currentStartDate')},
          start_time: {$lt: $scope.getReactively('currentEndDate')}
        },{
          sort: { type: 1, start_time: 1 }
        });
      });
    });

    /***** Subscribe barbers *****/
    $scope.$meteorSubscribe('barbers', $scope.getReactively('currentEndDate')).then(function() {
      $scope.barbers = $scope.$meteorCollection(function(){
        return Barbers.find({
          start_date: { $lte: $scope.getReactively('currentEndDate')}
        }, {
          sort: {name: 1}
        });
      });
    });

  });

  $scope.$meteorAutorun(function() {
    if($scope.getReactively('currentDate')) {
      // var mainDate = moment.utc(moment().toDate()).startOf('day'),
      //     currentStartDate = moment.utc(moment($scope.currentDate)).startOf('day');

      var mainDate = moment.tz(moment().toDate(), TIMEZONE).startOf('day'),
          currentStartDate = moment.tz($scope.currentDate, TIMEZONE).startOf('day');
      if(mainDate.isSame(currentStartDate)) {
        $scope.isMainDate = true;
      } else {
        $scope.isMainDate = false;
      }
    }
  });


  /*******************************/

  /***** Barber Functions *****/

  $scope.saveBooking = function(isValid) {
    if (!$scope.isMainDate) {
      Flash.create('danger', "Permission denied!!!", '');
      return;
    }

    if (!isValid) {
      return;
    }
    console.log("Valid...");
    // var current_time = moment.utc();
    var current_time = moment(); // get current time of system
    var hour = current_time.hour(),
        minute = current_time.minute(),
        second = current_time.second();

    $scope.newBooking.start_time = moment.utc($scope.currentStartDate).hour(hour).minute(minute).second(second)._d;
    console.log($scope.newBooking.start_time);

    $meteor.call('newWalkIn', $scope.newBooking).then(function(){
      Flash.create('success', 'Booked!!!', '');
      $scope.newBooking = {};
    }, function(error){
      Flash.create('danger', error.reason, '');
    })
  }

  $scope.getBarberName = function(barber_id) {
    if (barber_id == 'anyone') {
      return '1st available';
    } else {
      var barber = $filter('filter')($scope.barbers, {_id: barber_id})[0];
      return barber.name;
    }
  }

  $scope.verify = function (appointment_id) {
    if (!$scope.isMainDate) {
      Flash.create('danger', "Permission denied!!!", '');
      return;
    }
    var checbox_id = "#checkbox_" + appointment_id;
    var modalVerify = $modal.open({
      animation: true,
      windowClass: "verification-modal small-modal",
      templateUrl: 'client/walk/views/verification.html',
      controller: 'VerificationCtrl',
      size: 'sm'
    });

    modalVerify.result.then(function(valid){
      if (valid) {
        // var current_time = moment.utc();
        var current_time = moment();
        var hour = current_time.hour(),
            minute = current_time.minute(),
            second = current_time.second();

        var booking = {
          _id: appointment_id,
          end_time: moment.utc($scope.currentStartDate).hour(hour).minute(minute).second(second)._d
        }

        $meteor.call('endWalkIn', booking).then(function(){
          Flash.create('success', 'Completed!!!', '');
        }, function(error){
          Flash.create('danger', error.reason, '');
          $(checbox_id).prop('checked', false);
        })
      }
    }, function(){
      $(checbox_id).prop('checked', false);
    });
  }

}]);


