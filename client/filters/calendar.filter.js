angular
  .module('barber-booking')
  .filter('calendarFilter', calendarFilter);

function calendarFilter () {
  return function (time) {
    if (! time) return;

    return moment(time).format("MMMM D YYYY");
  }
}