angular.module('barber-booking').directive('uploadImage', ['$meteor', function($meteor){
  // Runs during compile
  return {
    // name: '',
    // priority: 1,
    // terminal: true,
    scope: {
      newImages: "=",
      imgSrc: "="
    }, // {} = isolate, true = child, false/undefined = no change
    controller: function($scope, $element, $attrs, $transclude) {
      $scope.images = $meteor.collectionFS(Images, false, Images);

      $scope.addImages = function(files) {
        if(files.length > 0) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $scope.$apply(function() {
              $scope.imgSrc = e.target.result;
            })
          }
          reader.readAsDataURL(files[0]);
          $scope.newImages.length = 0;

          var file = files[0];

          processImage(file, 300, 300, 1, function(data) {
            var img = new FS.File(data);

            Images.insert(img, function (err, fileObj) {
              // Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
              console.log(err);
              console.log(fileObj);
              $scope.newImages.push(fileObj);
            });
          });

          // $scope.images.save(files[0]).then(function(result) {
          //   $scope.newImages.push(result[0]._id);
          // });
        }
        else {
          $scope.imgSrc = undefined;
        }
      };

      $scope.removeImages = function() {
        $scope.imgSrc = undefined;
      };
    },
    // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
    restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
    // template: '',
    templateUrl: 'client/directives/upload-image.html',
    // replace: true,
    // transclude: true,
    // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
    link: function($scope, iElm, iAttrs, controller) {

    }
  };
}]);