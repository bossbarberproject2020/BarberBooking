angular.module('barber-booking').directive('equalField', ['$meteor', function($meteor){
  // Runs during compile
  return {
    require: 'ngModel',
    link: function($scope, element, attrs, ctrl) {
      if(!attrs.equalField) {
        return;
      }
     
      function matchValidator(value) {      

        $scope.$watch(attrs.equalField, function(newValue, oldValue) {
          var isValid = value === $scope.$eval(attrs.equalField);                    
          ctrl.$setValidity('equalField', isValid);
        });

        return value;
      }

      ctrl.$parsers.push(matchValidator);
    }
  };
}]);