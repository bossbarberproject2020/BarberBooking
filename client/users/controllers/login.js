angular.module("barber-booking").controller("LoginCtrl", ['$meteor', '$state',
  function ($meteor, $state) {
    var vm = this;

    var emails = {
      "admin": "admin@yopmail.com",
      "barber": "barber@yopmail.com"
    };

    vm.credentials = {
      userType: "barber",
      password: ''
    };

    vm.error = '';

    vm.login = function () {
      $meteor.loginWithPassword(emails[vm.credentials.userType], vm.credentials.password).then(
        function () {
          $state.go('barbers', {}, { reload: true });
        },
        function (err) {
          vm.error = 'Login error - ' + err;
        }
      );
    };

    vm.cancel = function () {
      $state.go('walk');
    }
  }
]);