angular.module('barber-booking').controller("SettingsCtrl", function($scope, $meteor, $rootScope, $modal, $modalInstance, $filter, Flash){

  $scope.$meteorSubscribe('settings').then(function() {
    $scope.settings = $scope.$meteorCollection(function() {
      return Settings.find({
      });
    });

    $scope.barber_code = Settings.findOne({name: "code"});
    $scope.barber_phone = Settings.findOne({name: "phone"});
    $scope.barber_time_out = Settings.findOne({name: "time_out"});
  });

  $scope.openPasswordModal = function(userType) {
    $modalInstance.close(true);
    var modalPasswordInstance = $modal.open({
      animation: true,
      windowClass: "new-setting-modal normal-modal",
      templateUrl: 'client/barbers/views/change-password.html',
      controller: 'ChangePasswordCtrl',
      resolve: {
        userType: function() {
          return userType;
        }
      }
    });

    modalPasswordInstance.result.then(function(){

    }, function(){

    });
  }

  $scope.saveSetting = function(isValid){

    if(!isValid){
      $scope.submitted = true;
      return;
    }

    $meteor.call("updateSettings", $scope.barber_code, $scope.barber_phone, $scope.barber_time_out).then(function(){
      Flash.create('success', "The settings has been updated successfully", '');
      $modalInstance.close(true);
    }, function(error){
      Flash.create('danger', error.reason, '');
    })
  }

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  }
})
