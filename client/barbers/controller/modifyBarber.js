angular.module('barber-booking').controller("ModifyBarberCtrl", function($scope, $meteor, $rootScope, $state, $modalInstance, $filter, Flash, barbers, currentBarber){
  $scope.currentBarber = currentBarber;
  $scope.newBarber = currentBarber ? Barbers.findOne(currentBarber._id) : {};
  $scope.images = $meteor.collectionFS(Images, false, Images);
  $scope.newBarberImages = [];

  if (!$scope.newBarber.images || !$scope.newBarber.images[0] || !$scope.images) {
    $scope.imgSrc = '';
  } else {
    var selectImage = $filter('filter')($scope.images, {_id: $scope.newBarber.images[0].id});
    $scope.imgSrc = selectImage[0] ? selectImage[0].url() : '';
  }
  // $scope.imgSrc = currentBarber ? ((currentBarber.images && currentBarber.images[0]) ? ($filter('filter')($scope.images, {_id: currentBarber.images[0].id})[0].url() : '') : '';

  $scope.saveBarber = function(isValid){
    // $scope.newBarber.owner = $rootScope.currentUser._id;

    if(!isValid){
      $scope.submitted = true;
      return;
    }

    if ($scope.newBarberImages) {
      if($scope.newBarberImages.length > 0) {

        $scope.newBarber.images = [];

        angular.forEach($scope.newBarberImages, function(image) {
          $scope.newBarber.images.push({id: image._id});
        });
      } else {
        // $scope.newBarber.images = null;
      }
    }

    action_name = currentBarber ? "updateBarber" : "newBarber";
    sucsess_mesg = currentBarber ? "The barber has been updated successfully" : "The barber has been created successfully" ;

    var val = $scope.newBarber.start_date;
    var time_utc = moment.utc().set('year', moment(val).get('year')).set('month', moment(val).get('month')).set('date', moment(val).get('date')).startOf('day').toDate();
    $scope.newBarber.start_date = time_utc;

    $meteor.call(action_name, $scope.newBarber).then(function(){
      Flash.create('success', sucsess_mesg, '');
      // Reset the form
      $scope.newBarberImages = [];
      $scope.newBarber = {};
      $scope.currentBarber = {};
      $modalInstance.close(true);
    }, function(error){
      $scope.existed = true;
      Flash.create('danger', error.reason, '');
    })
  }

  $scope.cancel = function(){
    //remove preview image
    if ($scope.newBarberImages[0]) {
      $meteor.call('removeBarberImage', $scope.newBarberImages[0]._id);
    }

    $scope.newBarberImages = [];
    $scope.newBarber = {};
    $scope.currentBarber = {};
    $modalInstance.dismiss('cancel');
  }
})
