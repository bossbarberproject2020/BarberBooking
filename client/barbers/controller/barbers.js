 angular.module("barber-booking").controller("BarbersCtrl", ['$scope', '$meteor', '$modal', '$filter', 'confirmService', '$state', 'Flash', '$q', '$compile', 'DTOptionsBuilder', 'DTColumnBuilder', 'Idle',
  function ($scope, $meteor, $modal, $filter, confirmService, $state, Flash, $q, $compile, DTOptionsBuilder, DTColumnBuilder, Idle) {

    var self = this;

  /******Config Idle-Watcher********/
  $scope.$meteorSubscribe('settings').then(function() {
    $scope.settings = $scope.$meteorCollection(function() {
      return Settings.find({
      });
    });

    $scope.barber_time_out = Settings.findOne({name: "time_out"}).value;

    Idle.setIdle($scope.barber_time_out * 60);
    Idle.watch();
  });

  $scope.$on('IdleStart', function() {
    // the user appears to have gone idle
    Meteor.logout();
    $state.go('walk');
  });

  $scope.$on('IdleEnd', function() {
      // the user has come back from AFK and is doing stuff. if you are warning them, you can use this to hide the dialog
    console.log("Come back...");
  });


  /* Initialize date */
    $scope.today = moment().toDate();
    $scope.currentDate = moment($scope.today).format("YYYY-MM-DD");
    $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
    $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
  /******************/

  /* Initialize header date picker*/
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 0
    };

    $scope.barberScope = function(){
      $scope.currentDate = moment($scope.today).format("YYYY-MM-DD");
      $scope.currentStartDate = moment.utc($scope.currentDate).startOf('day')._d;
      $scope.currentEndDate = moment.utc($scope.currentDate).endOf('day')._d;
    }
    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0) );
    };
  /******************************/

  $scope.work_ranges = WORK_HOURS;

  /***** Subscribe Functions *****/
  $scope.images = $meteor.collectionFS(Images, false, Images).subscribe('images');

  $scope.$meteorAutorun(function() {

    /***** Subscribe appointments *****/
    $scope.$meteorSubscribe('appointments', $scope.getReactively('currentStartDate'), $scope.getReactively('currentEndDate')).then(function() {
      $scope.appointments = $scope.$meteorCollection(function() {
        return Appointments.find({
          start_time: {$gte: $scope.getReactively('currentStartDate')},
          start_time: {$lt: $scope.getReactively('currentEndDate')}
        });
      });
    });

    /***** Subscribe barbers *****/
    $scope.$meteorSubscribe('barbers', $scope.getReactively('currentEndDate')).then(function() {
      $scope.barbers = $scope.$meteorCollection(function(){
        return Barbers.find({
          start_date: { $lte: $scope.getReactively('currentEndDate')}
        });
      });
    });

  });

  $scope.dtInstance = {};
  loadDataTables();


  $scope.$meteorAutorun(function() {
    Images.find({}).fetch();
    // console.log("image autorun");
    // console.log($scope.dtInstance);
    changeDataForTable();
  });

  $scope.$meteorAutorun(function() {
    $scope.getCollectionReactively('appointments');
    // console.log("appointment autorun");
    // console.log($scope.dtInstance);
    changeDataForTable();
  });

  $scope.$meteorAutorun(function() {
    Barbers.find({}).fetch();
    // console.log("barbers autorun");
    // console.log($scope.dtInstance);
    changeDataForTable();
  });

  $scope.$meteorAutorun(function() {
    $scope.getReactively('dtInstance');
    // console.log("dtInstance autorun");
    changeDataForTable();
  });

  // $scope.dtInstanceCallback = function(dtInstance) {
  //   $scope.dtInstance = dtInstance;
  //   console.log("call back");
  //   console.log($scope.dtInstance);
  //   changeDataForTable();
  // }
  /*******************************/


  /***** Barber Functions *****/
    $scope.isAdmin = function() {
      return Meteor.user() && Meteor.user().username == 'admin';
    }

    $scope.openSettingModal = function(){
      if (! $scope.isAdmin()) {
        return;
      }
      var modalSettingInstance = $modal.open({
        animation: true,
        windowClass: "new-setting-modal normal-modal",
        templateUrl: 'client/barbers/views/setting-modal.html',
        controller: 'SettingsCtrl'
      });

      modalSettingInstance.result.then(function(is_update){

      }, function(){

      });
    }

    $scope.openAddNewBarberModal = function(){
      if (! $scope.isAdmin()) {
        return;
      }
      var modalInstance = $modal.open({
        animation: true,
        windowClass: "new-barber-modal normal-modal",
        templateUrl: 'client/barbers/views/modify-barber-modal.html',
        controller: 'ModifyBarberCtrl',
        resolve: {
          barbers: function(){
            return $scope.barbers;
          },
          currentBarber: function(){
            return $scope.currentBarber;
          }
        }
      });

      modalInstance.result.then(function(is_update){
        $scope.currentBarber = undefined;
        if(is_update) {
          // changeDataForTable();
        }
      }, function(){
        $scope.currentBarber = undefined;
      });
    }

    $scope.remove = function(barber) {

      var modalOptions = {
        closeButtonText: 'Cancel',
        actionButtonText: 'Delete',
        headerText: 'Delete Barber ?',
        bodyText: 'Are you sure you want to delete this Barber?'
      };

      confirmService.showModal({}, modalOptions).then(function (result) {
        $meteor.call('removeBarber', barber).then(function (result) {
          Flash.create('success', "The Barber has been deleted successfully", '');
        })
      });
    }

    $scope.edit = function(barber){
      barber_instance = $scope.$meteorObject(Barbers, barber, false);
      $scope.currentBarber = barber_instance;
      $scope.openAddNewBarberModal();
    }

    $scope.getMainImage = function(images) {
      if (images && images.length && images[0] && images[0].id) {
        img = $filter('filter')($scope.images, {_id: images[0].id})[0];
        return (img ? img.url() : 'no-photo.png');
      } else {
        return 'no-photo.png';
      }
    };
  /**************************/

  /***** Appointment Functions *****/
    $scope.assignClient = function(barber, hour) {
      barber_instance = $scope.$meteorObject(Barbers, barber, false);

      var modalCustomer = $modal.open({
        animation: true,
        windowClass: "new-barber-modal normal-modal",
        templateUrl: 'client/barbers/views/customer.html',
        controller: 'CustomerCtrl',
      });

      modalCustomer.result.then(function(customer){
        newAppointment(barber_instance, hour, customer, 'assigned', 'Booked!!!');
      }, function(){

      });
    }

    $scope.goOut = function(barber, hour) {
      barber_instance = $scope.$meteorObject(Barbers, barber, false);
      newAppointment(barber_instance, hour, {}, 'go_out', 'Go out!!!');
    }

    $scope.removeAppointment = function(appointment) {
      appointment_instance = $scope.$meteorObject(Appointments, appointment, false);
      var pushId = appointment_instance.pushId,
          smsId = appointment_instance.smsId;
      $scope.appointments.remove(appointment_instance).then(function (result) {
        if (pushId) {
          Meteor.call('removeNotification', pushId, function (error, result) {});
        }
        if (smsId) {
          Meteor.call('removeSMS', smsId, function (error, result) {});
        }
        Flash.create('success', "The Appointment has been canceled successfully", '');
      });
    }

     $scope.leave = function(barber) {
      barber_instance = $scope.$meteorObject(Barbers, barber, false);

      var modalOptions = {
        closeButtonText: 'Cancel',
        actionButtonText: 'Leave',
        headerText: 'Leave Barber ?',
        bodyText: 'Are you sure you want to set leave this Barber?'
      };

      leaveBarber = {
        barber_id: barber_instance._id,
        current_date: $scope.currentStartDate
      }

      confirmService.showModal({}, modalOptions).then(function (result) {
        $meteor.call('leaveBarber', leaveBarber).then(function(){
          Flash.create('success', "Left!", '');
        }, function(error){
          Flash.create('danger', error.reason, '');
        })
      });
    }

    $scope.return = function(barber) {
      barber_instance = $scope.$meteorObject(Barbers, barber, false);

      returnBarber = {
        barber_id: barber_instance._id,
        current_date: $scope.currentStartDate
      }

      $meteor.call('returnBarber', returnBarber).then(function(){
        Flash.create('success', "Returned!", '');
      }, function(error){
        Flash.create('danger', error.reason, '');
      })
    }

    $scope.checkLeave = function(barber) {
      var appointment_count = Appointments.find({
        barber_id: barber._id,
        $and: [
          {start_time: { $gte: $scope.getReactively('currentStartDate')}},
          {start_time: { $lt: $scope.getReactively('currentEndDate')}}
        ]
      }).count();

      if(appointment_count == WORK_HOURS.length) {
        return true;
      }

      return false;
    }
  /*****************************/

  /***** Functions *****/
    function hasAppointment(barber, hour) {
      var book_time = hour.split(" - ");
      var start_time = book_time[0].split(":");
      var end_time = book_time[1].split(":");
      p_start_time = moment.utc($scope.currentStartDate).hour(start_time[0]).minute(start_time[1])._d;
      p_end_time = moment.utc($scope.currentStartDate).hour(end_time[0]).minute(end_time[1])._d;

      existed_appointment = Appointments.findOne({
        barber_id: barber._id,
        start_time: { $gte: p_start_time},
        start_time: { $lt: p_end_time},
        end_time: { $lte: p_end_time},
        end_time: { $gt: p_start_time}
      })

      return existed_appointment;
    }

    function newAppointment(barber, hour, customer, status, message) {
      if (_.isEmpty(customer)) {
        customer = {
          name: "",
          phone: "",
          noRemind: false
        }
      } else {
        status = "booked";
      }
      book_time = hour.split(" - ");
      start_time = book_time[0].split(":");
      end_time = book_time[1].split(":");

      new_appointment =  {
        barber_id: barber._id,
        start_time: moment.utc($scope.currentStartDate).hour(start_time[0]).minute(start_time[1])._d,
        end_time: moment.utc($scope.currentStartDate).hour(end_time[0]).minute(end_time[1])._d,
        status: status,
        time: hour,
        cus_name: customer.name || '',
        cus_phone: customer.phone || '',
        noRemind: customer.noRemind || false
      };

      $meteor.call('newAppointment', new_appointment).then(function(){
        Flash.create('success', message, '');
      }, function(error){
        Flash.create('danger', error.reason, '');
      })
    }

    function loadDataForTable() {
      var deferred = $q.defer();
      var currentCalendars = [];

      if($scope.barbers && $scope.appointments){
        var list_barbers = $scope.barbers;

        var calendar = {},
            hours_count =  WORK_HOURS.length,
            barbers_count = list_barbers.length;
        for (var i = 0; i < barbers_count; i++){
          calendar = {
            barber: list_barbers[i]
          };

          var get_day = $scope.today.getDay();
          for (var j = 0; j < hours_count; j++) {
            if (check_off(get_day, j)) {
              appointment = "off";
            } else{
              appointment = hasAppointment(calendar.barber, WORK_HOURS[j]);
            }
            data = {
              time: WORK_HOURS[j],
              appointment: appointment ? appointment : ""
            }

            calendar[WORK_HOURS[j]] = data;
          };

          calendar["action"] = "action";
          currentCalendars.push(calendar);
        }
      }

      deferred.resolve(currentCalendars);
      return deferred.promise;
    }

    function check_off(numDay, i) {
      switch(numDay) {
        case 0:
          return true;
        case 1:
        case 2:
        case 3:
        case 4:
          if(i < 2) {
            return true;
          }
          return false;
        case 5:
          return false;
        case 6:
          if (i >= WORK_HOURS.length - 2) {
            return true;
          }
          return false;
        default:
          return false
      }
    }

    function initColumns() {
      self.dtColumns = [
        DTColumnBuilder.newColumn('barber').withTitle('#').withClass('profile')
          .renderWith(function(data, type, full, meta) {
            return '<img width="100px" height="120px" alt="img" class="item-img" '+
                      'src="'+$scope.getMainImage(data.images)+'" ng-click="edit(\''+data._id+'\')"/>'+
                    '<div class="name clickable" ng-click="edit(\''+data._id+'\')">'+data.name+'</div>';
          })
      ];

      var hours_count =  WORK_HOURS.length;
      for (var i = 0; i < hours_count; i++) {
        self.dtColumns.push(DTColumnBuilder.newColumn(WORK_HOURS[i]).withTitle($filter('timeSlotFilter')(WORK_HOURS[i])).withClass('time-work')
          .renderWith(function(data, type, full, meta) {
            if(data.appointment == ''){
              return '<div>'+
                        '<button type="button" class="btn btn-w-m btn-assign clickable" ng-click="assignClient(\'' + full.barber._id + '\', \''+ data.time +'\')">Assign customer</button>'+
                        '<br/>'+
                        '<button type="button" class="btn btn-w-m btn-purple clickable" ng-click="goOut(\'' + full.barber._id + '\', \'' + data.time + '\')">Go out</button>'+
                      '</div>';
            } else {
              if(data.appointment == "off") {
                return  '<div>'+
                          "Not Work"+
                        '</div>'
              } else {
                var status = "<div>Booked</div>";
                if(data.appointment.status == "day_off") {
                  status = "<div>Day Off</div>";
                }
                if(data.appointment.status == "go_out") {
                  status = "<div>Go Out</div>"
                }
                if(data.appointment.status == "booked") {
                  var anyone = '';
                  var showPhone = ''
                  if(data.appointment.is_anyone) {
                    anyone = '(*) '
                  }
                  if (Meteor.user() && Meteor.user().username == 'admin') {
                    showPhone = '<div class="client client-phone">'+
                                  data.appointment.cus_phone +
                                '</div>';
                  }
                  status =  '<div class="client client-name">'+
                              anyone +
                              data.appointment.cus_name +
                            '</div>' + showPhone;
                  if (!data.appointment.cus_name && (!showPhone || !data.appointment.cus_phone)) {
                    status = "<div>Booked</div>";
                  }
                }
                return  '<div>'+
                          status +
                          '<div class="no-show clickable" ng-click="removeAppointment(\''+data.appointment._id+'\')">'+
                            'Cancel'+
                          '</div>'+
                        '</div>'
              }
            }
          }));
      }
      self.dtColumns.push(DTColumnBuilder.newColumn('barber').withTitle('Action').withClass("actions")
        .renderWith(function(data, type, full, meta) {
          var leaveAll = $scope.checkLeave(data);
          var action = '<div class="action clickable" ng-click="leave(\''+data._id+'\')">GO ON LEAVE</div>';
          if(leaveAll) {
            action = '<div class="action clickable" ng-click="return(\''+data._id+'\')">UN-LEAVE</div>';
          }
          return  action + '<div class="action clickable" ng-click="remove(\''+data._id+'\')">DELETE</div>';
        }));
    }

    function changeDataForTable(){

      if (_.isEmpty($scope.dtInstance)) {
        return;
      }
      // $scope.dtInstance.changeData(loadDataForTable());
      $scope.dtInstance.reloadData();
      // $scope.dtInstance.rerender();

    }

    function loadDataTables() {

      self.dtOptions =  DTOptionsBuilder.fromFnPromise(function(){
          return loadDataForTable();
        })
        .withOption('fnRowCallback',
         function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $compile(nRow)($scope);
         })
        .withOption('scrollY', "700px")
        .withOption('scrollX', true)
        .withOption('scrollCollapse', true)
        .withOption('bFilter', false)
        .withOption('bPaginate', false)
        .withOption('bInfo', false)
        .withOption('bSort', false)
        .withFixedColumns({
            leftColumns: 1,
            rightColumns: 1
        });

      // self.dtInstance = {};

      initColumns();

    }

    function loadWorkHourUlity() {
      var work_start = 8;
      var work_end = 18;

      var work_ranges = [];
      var work_hours = [];

      var start_time = moment().set('hour', work_start).set('minute', 0);
      var end_time = moment().set('hour', work_end).set('minute', 0);

      var time = '';
      while(start_time < end_time) {
        time = start_time.format("H:mm");
        work_ranges.push(time);
        start_time.add(30, 'm');
      }
      console.log(work_ranges);

      for (var i = 0; i < work_ranges.length - 1 ; i++) {
        var range = [work_ranges[i], work_ranges[i+1]];
        work_hours.push(range.join(" - "));
      }
      console.log(work_hours);

      return work_hours;
    }

  /*****************************************/
}]);


