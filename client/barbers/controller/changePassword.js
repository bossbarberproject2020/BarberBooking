angular.module('barber-booking').controller("ChangePasswordCtrl", function($scope, $meteor, $modalInstance, $filter, Flash, userType){

  $scope.login = { };
  $scope.userType = userType;
  $scope.savePass = function(isValid){

    if(!isValid){
      $scope.submitted = true;
      return;
    }

    if (!Meteor.user() || !(Meteor.user().username == 'admin')) {
      return;
    }

    if ($scope.userType == "admin") {
      $meteor.changePassword($scope.login.current_password, $scope.login.new_password).then(
        function () {
          Flash.create('success', "The admin password has been updated successfully", '');
          $modalInstance.close(true);
        },
        function (err) {
          Flash.create('danger', err.reason, '');
        }
      );
    } else {
      $meteor.call('setBarberPassword', $scope.login.new_password).then(function(){
        Flash.create('success', "The barber password has been updated successfully", '');
        $modalInstance.close(true);
      }, function(error){
        Flash.create('danger', error.reason, '');
      });
    }

  }

  $scope.cancel = function(){
    $modalInstance.dismiss('cancel');
  }
})
