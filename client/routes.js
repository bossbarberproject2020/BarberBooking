
angular.module("barber-booking").run(function ($rootScope, $state, $timeout) {
  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    // We can catch the error thrown when the $requireUser promise is rejected
    // and redirect the user back to the main page
    if (error === 'AUTH_REQUIRED') {
      $state.go('walk');
      //Flash.create('warning', "You need to log in before continuing.", '');
    }
  });
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
    if ((toState.url == '/login') && $rootScope.currentUser) {
      $timeout(function () {
        $state.go('barbers');
      });
    }
  });
});

angular.module("barber-booking").config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
  $locationProvider.html5Mode(true);

  $stateProvider
    // .state('booking', {
    //   url: '/booking',
    //   templateUrl: 'client/booking/views/booking.html',
    //   controller: 'BookingCtrl',
    //   controllerAs: 'bookctrl'
    // })
    .state('walk', {
      url: '/walk',
      templateUrl: 'client/walk/views/walk.html',
      controller: 'WalkCtrl',
      controllerAs: 'walkctrl'
    })
    .state('barbers', {
      url: '/barbers',
      templateUrl: 'client/barbers/views/barbers-list.html',
      controller: 'BarbersCtrl',
      controllerAs: 'bactrl',
      resolve: {
        "currentUser": function ($meteor) {
          return $meteor.requireUser();
        }
      }
    })
    .state('schedule', {
      url: '/schedule',
      templateUrl: 'client/schedule/views/schedule.html',
      controller: 'ScheduleCtrl',
      controllerAs: 'schc'
    })
    .state('login', {
      url: '/login',
      templateUrl: 'client/users/views/login.html',
      controller: 'LoginCtrl',
      controllerAs: 'lc'

    })
    .state('logout', {
      url: '/logout',
      resolve: {
        "logout": function($meteor, $state) {
          return $meteor.logout().then(function(){
            $state.go('walk');
          }, function(err){
            console.log('logout error - ', err);
          });
        }
      }
    });

  $urlRouterProvider.otherwise("/walk");
});

