
var barberShop = angular.module('barber-booking',[
  'angular-meteor',
  'ui.router',
  'ui.bootstrap',
  'ngFileUpload',
  'flash',
  'uiGmapgoogle-maps',
  'ngGeolocation',
  'datatables',
  'datatables.fixedcolumns',
  'ngIdle',
  'ngIntlTelInput'
]);

function onReady() {
  angular.bootstrap(document, ['barber-booking'], {
    strictDi: true
  });

  // Full height of sidebar
  function fix_height() {
      var heightWithoutNavbar = $("body > #wrapper").height() - 61;
      $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");
  }
  fix_height();

  $(window).bind("load resize click scroll", function() {
      if(!$("body").hasClass('body-small')) {
          fix_height();
      }
  })

}

barberShop.config(function(ngIntlTelInputProvider) {
  ngIntlTelInputProvider.set({initialCountry: 'us'});
  ngIntlTelInputProvider.set({utilsScript: "/client/lib/intl-tel-input/utils.js"});
  ngIntlTelInputProvider.set({onlyCountries: ['us']});
});

if (Meteor.isCordova)
  angular.element(document).on("deviceready", onReady);
else
  angular.element(document).ready(onReady);

// For demo purpose - animation css script
function animationHover(element, animation) {
  element = $(element);
  element.hover(
    function() {
      element.addClass('animated ' + animation);
    },
    function() {
      //wait for animation to finish before removing classes
      window.setTimeout(function() {
        element.removeClass('animated ' + animation);
      }, 2000);
    });
}

// Minimalize menu when screen is less than 768px
$(function() {
  $(window).bind("load resize", function() {
    if ($(this).width() < 769) {
      $('body').addClass('body-small')
    } else {
      $('body').removeClass('body-small')
    }
  })
})

function SmoothlyMenu() {
  if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
    // Hide menu in order to smoothly turn on when maximize menu
    $('#side-menu').hide();
    // For smoothly turn on menu
    setTimeout(
      function() {
        $('#side-menu').fadeIn(500);
      }, 100);
  } else {
    // Remove all inline style from jquery fadeIn function to reset menu state
    $('#side-menu').removeAttr('style');
  }
}

// Dragable panels
function WinMove() {
  $("div.ibox").not('.no-drop')
    .draggable({
      revert: true,
      zIndex: 2000,
      cursor: "move",
      handle: '.ibox-title',
      opacity: 0.8,
      drag: function() {
        var finalOffset = $(this).offset();
        var finalxPos = finalOffset.left;
        var finalyPos = finalOffset.top;
        // Add div with above id to see position of panel
        $('#posX').text('Final X: ' + finalxPos);
        $('#posY').text('Final Y: ' + finalyPos);
      },
    })
    .droppable({
      tolerance: 'pointer',
      drop: function(event, ui) {
        var draggable = ui.draggable;
        var droppable = $(this);
        var dragPos = draggable.position();
        var dropPos = droppable.position();
        draggable.swap(droppable);
        setTimeout(function() {
          var dropmap = droppable.find('[id^=map-]');
          var dragmap = draggable.find('[id^=map-]');
          if (dragmap.length > 0 || dropmap.length > 0) {
            dragmap.resize();
            dropmap.resize();
          } else {
            draggable.resize();
            droppable.resize();
          }
        }, 50);
        setTimeout(function() {
          draggable.find('[id^=map-]').resize();
          droppable.find('[id^=map-]').resize();
        }, 250);
      }
    });
}
jQuery.fn.swap = function(b) {
  b = jQuery(b)[0];
  var a = this[0];
  var t = a.parentNode.insertBefore(document.createTextNode(''), a);
  b.parentNode.insertBefore(a, b);
  t.parentNode.insertBefore(b, t);
  t.parentNode.removeChild(t);
  return this;
};

