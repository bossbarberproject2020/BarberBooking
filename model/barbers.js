Barbers = new Mongo.Collection("barbers");

Meteor.methods({
  newBarber: function (barber) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }

    existed_barber = Barbers.findOne({name: barber.name});
    if (existed_barber) {
      throw new Meteor.Error(403, "Barbers with name '" + barber.name + "' has been existed!");
    }

    barber.created_at = moment.utc().toDate();

    var barberID = Barbers.insert(barber);
    return barberID;
  },

  updateBarber: function (barber) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }

    var existed_barber = Barbers.findOne({name: barber.name});
    if (existed_barber && barber._id != existed_barber._id) {
      throw new Meteor.Error(403, "Barbers with name '" + barber.name + "' has been existed!");
    }

    var id = barber._id;

    existed_barber = Barbers.findOne({_id: id});
    var oldImageID = (existed_barber.images && existed_barber.images[0]) ? existed_barber.images[0].id : '';
    var newImageID = (barber.images && barber.images[0]) ? barber.images[0].id : '';

    if (oldImageID != newImageID) {
      Images.remove({_id: oldImageID});
    }


    delete barber["_id"];
    var barberID = Barbers.update({_id: id}, {'$set': barber}, {multi: true});
    return barberID;
  },

  removeBarber: function (barber_id) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }


    var existed_barber = Barbers.findOne({_id: barber_id});
    if (existed_barber) {
      //Remove Images
      var imageID = (existed_barber.images && existed_barber.images[0]) ? existed_barber.images[0].id : '';
      Images.remove({_id: imageID});

      // Remove Barber
      Barbers.remove({
        _id: barber_id,
      });

      // Remove Appointment
      Appointments.remove({
        barber_id: barber_id
      })
    }
  },

  removeBarberImage: function(imageID) {
    refCount = Barbers.find({"images.id": imageID}).count();
    if (refCount == 0) {
      Images.remove({_id: imageID});
    }
  }
})