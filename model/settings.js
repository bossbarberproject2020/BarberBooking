Settings = new Mongo.Collection("settings");


Meteor.methods({

  updateCode: function (setting) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }

    var id = setting._id;
    delete setting["_id"];

    var settingID = Settings.update({_id: id}, {'$set': setting}, {multi: true});
    return settingID;
  },


  verifyBarberCode: function(digest) {
    check(digest, String);

    if (Meteor.isServer) {
      var user = Accounts.findUserByEmail('barber@yopmail.com');
      var password = {digest: digest, algorithm: 'sha-256'};
      var result = Accounts._checkPassword(user, password);
      return result.error == null;
    }
  },

  updateSettings: function (code, phone, time_out) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }

    var code_id = code._id;
    delete code["_id"];
    var codeID = Settings.update({_id: code_id}, {'$set': code}, {multi: true});

    var phone_id = phone._id;
    delete phone["_id"];
    var phoneID = Settings.update({_id: phone_id}, {'$set': phone}, {multi: true});

    var time_out_id = time_out._id;
    delete time_out["_id"];
    var time_outID = Settings.update({_id: time_out_id}, {'$set': time_out}, {multi: true});

    return codeID & phoneID & time_outID;
  }
})