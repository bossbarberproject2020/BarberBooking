Appointments = new Mongo.Collection("appointments");

Appointments.allow({
  remove: function (userId) {
    return Meteor.userId();
  }
});

Meteor.methods({
  newWalkIn: function (appointment){
    check(appointment, {
      barber_id: String,
      cus_name: String,
      start_time: Date
    });

    // appointment.start_time =  moment.utc()._d;
    appointment.type = 'W';
    appointment.status = 'booked';

    notAvailableAppointment = Appointments.findOne({
      barber_id: appointment.barber_id,
      status: { $in: ['day_off', 'go_out'] },
      $and: [
        {start_time: { $lte: appointment.start_time }},
        {end_time: { $gt: appointment.start_time }},
      ]
    });


    if (notAvailableAppointment) {
      throw new Meteor.Error('invalid-data',
        'The barber is not working right now!');
    }

    var appointmentID = Appointments.insert(appointment);
    return appointmentID;
  },

  endWalkIn: function (appointment){
    check(appointment, {
      _id: String,
      end_time: Date
    });

    Appointments.update({_id: appointment._id}, {'$set': {status: "completed", end_time: appointment.end_time}}, {multi: true});
  },

  newBookingAppointment: function (appointment){
    check(appointment, {
      barber_id: String,
      start_time: Date,
      end_time: Date,
      status: String,
      time: String,
      cus_name: String,
      cus_phone: String
    });
    appointment.type = 'A';

    if (appointment.start_time >= appointment.end_time) {
      throw new Meteor.Error('invalid-data',
        'Invalid Data!');
    }

    var toDate = moment.utc().startOf('day');
    var start_time = moment.utc(appointment.start_time);
    if (start_time < moment.utc(toDate)) {
      throw new Meteor.Error('invalid-data',
        "Can't use this action in the past!");
    }

    if ((start_time._d.getDay() == 6 && start_time.get('hour') >= 17) || start_time._d.getDay() == 0 || start_time.get('hour') < 7) {
      throw new Meteor.Error('invalid-data',
        "Office doesn't work in this time!");
    }

    var existed_appointment = null;

    if (appointment.barber_id == "anyone") {
      var barbers = Barbers.find({}).fetch();
      for (let i=0; i < barbers.length; i ++) {
        existed_appointment = Appointments.findOne({
          barber_id: barbers[i]._id,
          start_time: { $gte: appointment.start_time },
          start_time: { $lt: appointment.end_time },
          end_time: { $lte: appointment.end_time },
          end_time: { $gt: appointment.start_time },
        });

        if(!existed_appointment) {
          appointment.barber_id = barbers[i]._id;
          break;
        }
      }
      appointment.is_anyone = true;
    } else {
      var existed_barber = Barbers.findOne({_id: appointment.barber_id});
      if(!existed_barber) {
        throw new Meteor.Error('invalid-data',
          "The barber is not existed!");
      }

      existed_appointment = Appointments.findOne({
        barber_id: appointment.barber_id,
        start_time: { $gte: appointment.start_time },
        start_time: { $lt: appointment.end_time },
        end_time: { $lte: appointment.end_time },
        end_time: { $gt: appointment.start_time },
      });
    }

    if (existed_appointment) {
      throw new Meteor.Error(403, "This range hour has been ready booked!");
    }

    var appointmentID = Appointments.insert(appointment);
    return appointmentID;
  },

  newAppointment: function (appointment) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }

    check(appointment, {
      barber_id: String,
      start_time: Date,
      end_time: Date,
      status: String,
      time: String,
      cus_name: String,
      cus_phone: String,
      noRemind: Boolean
    });

    appointment.type = 'A';

    if (appointment.start_time >= appointment.end_time) {
      throw new Meteor.Error('invalid-data',
        'Invalid Data!');
    }

    /* Book use UTC as start_time  ex: 2016-18-02 10:00 (UTC)*/
    // toDate = moment.utc().startOf('day')._d;
    // start_time = moment.utc(appointment.start_time);
    // if (start_time._d < moment.utc(toDate)._d) {
    var toDate = moment.utc(moment.tz(moment().toDate(), TIMEZONE).format("YYYY-MM-DD")).startOf('day'),
          start_time = moment.tz(appointment.start_time, TIMEZONE);
    if (start_time < toDate) {
      throw new Meteor.Error('invalid-data',
        "Can't use this action in the past!");
    }

    start_time = moment.utc(appointment.start_time);
    if ((start_time._d.getDay() == 6 && start_time.get('hour') >= 17) || start_time._d.getDay() == 0 || start_time.get('hour') < 7) {
      throw new Meteor.Error('invalid-data',
        "Office doesn't work in this time!");
    }

    var existed_barber = Barbers.findOne({_id: appointment.barber_id});
    if(!existed_barber) {
      throw new Meteor.Error('invalid-data',
        "The barber is not existed!");
    }

    existed_appointment = Appointments.findOne({
      barber_id: appointment.barber_id,
      start_time: { $gte: appointment.start_time },
      start_time: { $lt: appointment.end_time },
      end_time: { $lte: appointment.end_time },
      end_time: { $gt: appointment.start_time }
    });

    if (existed_appointment) {
      throw new Meteor.Error(403, "This range hour has been ready booked!");
    }

    var appointmentID = Appointments.insert(appointment);

    Meteor.call('sendSMS', appointmentID);

    return appointmentID;
  },

  leaveBarber: function (leaveBarber) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }

    check(leaveBarber, {
      barber_id: String,
      current_date: Date
    });

    var existed_barber = Barbers.findOne({_id: leaveBarber.barber_id});
    if(!existed_barber) {
      throw new Meteor.Error('invalid-data',
        "The barber is not existed!");
    }


    /* Book use start of date UTC as current_date  ex: 2016-18-02 00:00 (UTC) => convert system time to UTC*/
    // toDate = moment.utc().startOf('day')._d;
    toDate = moment.utc(moment.tz(moment().toDate(), TIMEZONE).format("YYYY-MM-DD")).startOf('day')._d;
    if (moment.utc(leaveBarber.current_date)._d < moment.utc(toDate)._d) {
      throw new Meteor.Error('invalid-data',
        "Can't use this action in the past!");
    }

    WORK_HOURS.forEach(function (hour) {
      var book_time = hour.split(" - ");
      var start_time = book_time[0].split(":");
      var end_time = book_time[1].split(":");
      p_start_time = moment.utc(leaveBarber.current_date).hour(start_time[0]).minute(start_time[1])._d;
      p_end_time = moment.utc(leaveBarber.current_date).hour(end_time[0]).minute(end_time[1])._d;

      existed_appointment = Appointments.findOne({
        barber_id: leaveBarber.barber_id,
        start_time: { $gte: p_start_time },
        start_time: { $lt: p_end_time },
        end_time: { $lte: p_end_time },
        end_time: { $gt: p_start_time }
      });

      if (existed_appointment) {
        Appointments.update({_id: existed_appointment._id}, {'$set': {status: "day_off"}}, {multi: true});
      } else {
        Appointments.insert({
          barber_id: leaveBarber.barber_id,
          start_time: p_start_time,
          end_time: p_end_time,
          status: "day_off",
          time: hour
        })
      }

    });
  },

  returnBarber: function (returnBarber) {
    if (! this.userId) {
      throw new Meteor.Error('not-logged-in',
        'Must be logged in!');
    }

    check(returnBarber, {
      barber_id: String,
      current_date: Date
    });

    /* Book use start of date UTC as current_date  ex: 2016-18-02 00:00 (UTC) => convert system time to UTC*/
    // toDate = moment.utc().startOf('day')._d;
    toDate = moment.utc(moment.tz(moment().toDate(), TIMEZONE).format("YYYY-MM-DD")).startOf('day')._d;
    if (moment.utc(returnBarber.current_date)._d < moment.utc(toDate)._d) {
      throw new Meteor.Error('invalid-data',
        "Can't use this action in the past!");
    }

    p_start_time = moment.utc(returnBarber.current_date).startOf('day')._d;
    p_end_time = moment.utc(returnBarber.current_date).endOf('day')._d;

    Appointments.remove({
      barber_id: returnBarber.barber_id,
      start_time: { $gte: p_start_time },
      start_time: { $lt: p_end_time },
      status: "day_off"
    });
  }
})