Meteor.publish("barbers", function (date) {
  return Barbers.find({start_date: {$lte: date}});
});

Meteor.publish('images', function() {
  return Images.find({});
});


/*publish for admin table page*/
Meteor.publish("appointments", function (start_date, end_date) {
  return Appointments.find({
    $and: [
      {start_time: {$gte: moment.utc(start_date)._d}},
      {start_time: {$lt: moment.utc(end_date)._d}}
    ],
    type: {$ne: 'W'},
    barber_id: {$ne: 'anyone'}
  });
});

Meteor.publish("booking_appointments", function (start_date, end_date) {

  return Appointments.find({
    $and: [
      {start_time: {$gte: moment.utc(start_date)._d}},
      {start_time: {$lt: moment.utc(end_date)._d}},
    ],
    status: {$in: ['booked', "completed"]}
  }, {
    sort: { type: -1, start_time: 1 }
  });
});


Meteor.publish('settings', function() {
  return Settings.find({});
});