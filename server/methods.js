Meteor.methods({
  setBarberPassword: function(newPassword) {
    check(newPassword, String);
    if (!Meteor.user() || !(Meteor.user().username == 'admin')) {
      throw new Meteor.Error('permission-deny',
        'Permission deny!');
    }
    var user = Meteor.users.findOne({ username: 'barber' });
    if (!user) {
      throw new Meteor.Error('invalid-data',
        'The barber account is not exist!');
    }
    return Accounts.setPassword(user._id, newPassword);
  },
  sendSMS: function(appointmentID) {
    check(appointmentID, String);

    let appointment = Appointments.findOne({_id: appointmentID});
    if (!appointment || !appointment.cus_phone || appointment.noRemind) {
      return;
    }

    let barberName = appointment.barber_id;
    if (barberName != 'anyone') {
      let barber = Barbers.findOne({_id: appointment.barber_id});
      barberName = (barber && barber.name) || 'anyone';
    }

    let utcTime = moment.utc(appointment.start_time),
        bookTimeStr = utcTime.format("hh:mm A");
        pushTime = moment(utcTime.subtract(1, 'hours').format().replace('+00:00', UTC_OFFSET))._d,
        message = 'You have an appointment with ' +  barberName + ' at '+ bookTimeStr + ' today!';

    if (pushTime < moment()._d) {
      return;
    }

    let smsId = SMSQueue.insert({
      message: "Men's Den App: " + message,
      sent: false,
      sending: 0,
      delayUntil: pushTime,
      to: appointment.cus_phone
    })

    if (smsId) {
      Appointments.update({
        _id: appointmentID
      }, {
        $set: {
          smsId: smsId
        }
      });
    }
  }
});
