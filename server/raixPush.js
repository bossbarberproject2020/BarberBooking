// RaixTokens = new Mongo.Collection('_raix_push_app_tokens');
RaixNotifications = new Mongo.Collection('_raix_push_notifications');
SMSQueue = new Mongo.Collection("smsQueue");

Meteor.methods({
  removeNotification: function(pushId) {
    check(pushId, String);
    RaixNotifications.remove({
      _id: pushId
    });
  },
  removeSMS: function(smsId) {
    check(smsId, String);
    SMSQueue.remove({
      _id: smsId
    })
  }
});