Meteor.startup(function() {

  if (Meteor.users.find().count() === 0) {
    Accounts.createUser({
      username: 'admin',
      email: 'admin@yopmail.com',
      password: '12345678',
      profile: {
        first_name: 'admin',
        last_name: 'admin',
        company: 'FRT',
      }
    });

    Accounts.createUser({
      username: 'barber',
      email: 'barber@yopmail.com',
      password: 'barber123',
      profile: {
        first_name: 'barber',
        last_name: 'barber',
        company: 'FRT',
      }
    });
  }

  if (Settings.find({ name: "code" }).count() === 0) {
    Settings.insert({
      name: "code",
      value: "123"
    });
  }

  if (Settings.find({ name: "phone" }).count() === 0) {
    Settings.insert({
      name: "phone",
      value: "12345678910"
    });
  }

  if (Settings.find({ name: "time_out" }).count() === 0) {
    Settings.insert({
      name: "time_out",
      value: "2" /*minutes*/
    });
  }

  // Migration update

  // let appointments = Appointments.find({
  //   $or: [
  //     { smsId: { $exists: true } },
  //     { pushId: { $exists: true } }
  //   ]
  // });
  // appointments.forEach(function(appointment) {
  //   let utcTime = moment.utc(appointment.start_time),
  //     pushTime = moment(utcTime.subtract(1, 'hours').format().replace('+00:00', UTC_OFFSET))._d;
  //   if (appointment.cus_phone && !appointment.noRemind && appointment.smsId) {
  //     SMSQueue.update({
  //       _id: appointment.smsId
  //     }, {
  //       $set: {
  //         delayUntil: pushTime,
  //         updateTimeZone: true
  //       }
  //     });
  //   }

  //   if (appointment.pushId) {
  //     RaixNotifications.update({
  //       _id: appointment.pushId
  //     }, {
  //       $set: {
  //         delayUntil: pushTime,
  //         updateTimeZone: true
  //       }
  //     });
  //   }
  // });

  process.env.MAIL_URL = "smtp://aegcrmvietnam%40gmail.com:1qazXSW@3edc@smtp.gmail.com:587/";

});
